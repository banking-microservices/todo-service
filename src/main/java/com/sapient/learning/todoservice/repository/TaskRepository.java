package com.sapient.learning.todoservice.repository;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.sapient.learning.todoservice.entity.TaskEntity;

/**
 * @author - chiranjitbhattacharya
 * @created - 13/07/2021
 */
public interface TaskRepository extends MongoRepository<TaskEntity, String> {
	
	List<TaskEntity> findByUsername(String username);
}
