package com.sapient.learning.todoservice.api.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.sapient.learning.todoservice.api.TaskApi;
import com.sapient.learning.todoservice.dto.TaskDto;
import com.sapient.learning.todoservice.service.TaskService;

import io.swagger.v3.oas.annotations.headers.Header;

/**
 * @author - chiranjitbhattacharya
 * @created - 13/07/2021
 */
@RestController
@CrossOrigin(origins = "*")
@RequestMapping( value = "/api/v1" )
public class TaskController implements TaskApi {


    @Autowired
    private TaskService taskService;



    @Override
    @GetMapping( value = "/tasks", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TaskDto>> findByUsername( @RequestHeader(name = "username") String username) {
    	System.out.println("ID: " + username);
        return new ResponseEntity<>(taskService.findByUsername(username), HttpStatus.OK);
    }

    @Override
    @GetMapping( value = "/tasks/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TaskDto> findById(@PathVariable( value = "id", required = true ) String taskId) {
        return new ResponseEntity<>(taskService.findById(taskId), HttpStatus.OK);
    }

    @Override
    @PostMapping( value = "/tasks", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TaskDto> create(@Valid @RequestBody(required = true) TaskDto taskDto) {
        return new ResponseEntity<>(taskService.create(taskDto), HttpStatus.CREATED);
    }

    @Override
    @PutMapping( value = "/tasks", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TaskDto> update(@Valid @RequestBody(required = true) TaskDto taskDto) {
        return new ResponseEntity<>(taskService.update(taskDto), HttpStatus.OK);
    }

    @Override
    @DeleteMapping( value = "/tasks/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<TaskDto> delete(@PathVariable( value = "id", required = true ) String taskId) {
        return new ResponseEntity<>(taskService.delete(taskId), HttpStatus.OK);
    }
}
