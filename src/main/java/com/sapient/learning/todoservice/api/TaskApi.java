package com.sapient.learning.todoservice.api;

import com.sapient.learning.todoservice.dto.ErrorResponse;
import com.sapient.learning.todoservice.dto.TaskDto;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * @author - chiranjitbhattacharya
 * @created - 13/07/2021
 */

@Tag(name = "Tasks", description = "The Tasks APIs")
public interface TaskApi {



    @Operation(summary = "Find all tasks", description = "Find all tasks for the current user", tags = { "task" })
    @ApiResponses(
            value = {
                @ApiResponse(responseCode = "200", description = "Success", content = @Content(schema = @Schema(implementation = TaskDto.class))),
                @ApiResponse(responseCode = "500", description = "Internal Server Error")
            })
    ResponseEntity<List<TaskDto>> findByUsername(String username);



    @Operation(summary = "Find task by Id", description = "Find task by Id for the current user", tags = { "task" })
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Success", content = @Content(schema = @Schema(implementation = TaskDto.class))),
            })
    ResponseEntity<TaskDto> findById(String taskId);




    @Operation(summary = "Create a new task", description = "Create a new task for the current user", tags = { "task" })
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Success", content = @Content(schema = @Schema(implementation = TaskDto.class))),
                    @ApiResponse(responseCode = "400", description = "Validation Error", content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
            })
    ResponseEntity<TaskDto> create(TaskDto taskDto);





    @Operation(summary = "Update a task", description = "Update an existing task for the current user", tags = { "task" })
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Success", content = @Content(schema = @Schema(implementation = TaskDto.class))),
                    @ApiResponse(responseCode = "400", description = "Validation Error", content = @Content(schema = @Schema(implementation = ErrorResponse.class)))
            })
    ResponseEntity<TaskDto> update(TaskDto taskDto);





    @Operation(summary = "Delete a task by Id", description = "Delete an existing task for the current user", tags = { "task" })
    @ApiResponses(
            value = {
                    @ApiResponse(responseCode = "200", description = "Success", content = @Content(schema = @Schema(implementation = TaskDto.class)))
            })
    ResponseEntity<TaskDto> delete(String taskId);
}
