package com.sapient.learning.todoservice.service;

import com.sapient.learning.todoservice.dto.TaskDto;
import com.sapient.learning.todoservice.entity.TaskEntity;

import java.util.List;

/**
 * @author - chiranjitbhattacharya
 * @created - 13/07/2021
 */
public interface TaskService {

    List<TaskDto> findByUsername(String username);

    TaskDto findById(String id);

    TaskDto create(TaskDto taskDto);

    TaskDto update(TaskDto taskDto);

    TaskDto delete(String id);
}
