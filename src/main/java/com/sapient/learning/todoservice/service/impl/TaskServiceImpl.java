package com.sapient.learning.todoservice.service.impl;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sapient.learning.todoservice.activitylogger.ActivityPublisher;
import com.sapient.learning.todoservice.activitylogger.EventType;
import com.sapient.learning.todoservice.activitylogger.TaskEvent;
import com.sapient.learning.todoservice.dto.TaskDto;
import com.sapient.learning.todoservice.entity.TaskEntity;
import com.sapient.learning.todoservice.exception.ResourceNotFoundException;
import com.sapient.learning.todoservice.repository.TaskRepository;
import com.sapient.learning.todoservice.service.TaskService;
import com.sapient.learning.todoservice.util.ConvertorFunctions;

/**
 * @author - chiranjitbhattacharya
 * @created - 13/07/2021
 */
@Service
public class TaskServiceImpl implements TaskService {


    @Autowired
    private TaskRepository taskRepository;
    
    @Autowired
    private ActivityPublisher activtyPublisher;

    @Override
    public List<TaskDto> findByUsername(String username) {
        return taskRepository.findByUsername(username)
                .stream()
                .map(ConvertorFunctions::toTaskDto)
                .collect(Collectors.toList());
    }

    @Override
    public TaskDto findById(String id) {
        var task = taskRepository.findById(id).orElseThrow( () -> new ResourceNotFoundException("Invalid Task Id."));
        return ConvertorFunctions.toTaskDto(task);
    }

    @Override
    public TaskDto create(TaskDto taskDto) {
    	
    	var savedTask = taskRepository.save(ConvertorFunctions.toTaskEntity(taskDto));
    	
    	// Publish an event to Kafka.
    	activtyPublisher.publish(prepareEvent(EventType.TASK_CREATED, "SYS_USER", savedTask));
    	
        return ConvertorFunctions.toTaskDto(savedTask);
    }

    @Override
    public TaskDto update(TaskDto taskDto) {
    	
    	if(taskDto.get_id() == null) {
    		throw new ResourceNotFoundException("Invalid Task Id.");
    	}
    	
    	var updatedTask = taskRepository.save(ConvertorFunctions.toTaskEntity(taskDto));
    	
    	// Publish an event to Kafka.
    	activtyPublisher.publish(prepareEvent(EventType.TASK_UPDATED, "SYS_USER", updatedTask));
    	
        return ConvertorFunctions.toTaskDto(updatedTask);
    }

    @Override
    public TaskDto delete(String id) {
        var taskToBeDeleted = findById(id);

        taskRepository.deleteById(id);
    	
    	// Publish an event to Kafka.
    	activtyPublisher.publish(prepareEvent(EventType.TASK_DELETED, "SYS_USER", taskToBeDeleted));
        
        return taskToBeDeleted;
    }
    
   
    private TaskEvent prepareEvent(EventType eventType, String username, TaskEntity task) {
    	return new TaskEvent(eventType, task.get_id().toHexString(), task.getTitle(), LocalDateTime.now(), username);
    }
    private TaskEvent prepareEvent(EventType eventType, String username, TaskDto task) {
    	return new TaskEvent(eventType, task.get_id().toHexString(), task.getTitle(), LocalDateTime.now(), username);
    }
}
