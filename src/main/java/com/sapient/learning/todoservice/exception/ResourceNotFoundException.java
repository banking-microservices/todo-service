package com.sapient.learning.todoservice.exception;

/**
 * @author - chiranjitbhattacharya
 * @created - 13/07/2021
 */
public class ResourceNotFoundException extends RuntimeException {


    public ResourceNotFoundException(String message) {
        super(message);
    }
}
