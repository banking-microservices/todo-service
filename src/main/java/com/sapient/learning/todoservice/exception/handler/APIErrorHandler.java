package com.sapient.learning.todoservice.exception.handler;

import com.sapient.learning.todoservice.dto.ErrorResponse;
import com.sapient.learning.todoservice.dto.FieldError;
import com.sapient.learning.todoservice.exception.ResourceNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.stream.Collectors;

/**
 * @author - chiranjitbhattacharya
 * @created - 13/07/2021
 */

@ControllerAdvice
public class APIErrorHandler extends ResponseEntityExceptionHandler {



    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(
            MethodArgumentNotValidException exception,
            HttpHeaders headers,
            HttpStatus status,
            WebRequest request) {


        var fieldErrors = exception.getBindingResult().getFieldErrors().stream().map( err -> new FieldError(err.getField(), err.getDefaultMessage()))
                .collect(Collectors.toList());

        var detailErrorMessage = ErrorResponse.builder()
                .path(((ServletWebRequest)request).getRequest().getRequestURI())
                .code(status.value())
                .status(status.name())
                .message("Validation Failed!")
                .timestamp(LocalDateTime.now())
                .errors(fieldErrors).build();

        return new ResponseEntity(detailErrorMessage, headers, status);
    }


    @ExceptionHandler(ResourceNotFoundException.class)
    protected ResponseEntity<Object> handleResourceNotFoundException(
            ResourceNotFoundException exception,
            WebRequest request) {


        var detailErrorMessage = ErrorResponse.builder()
                .path(((ServletWebRequest)request).getRequest().getRequestURI())
                .code(400)
                .status("BAD_REQUEST")
                .message(exception.getMessage())
                .timestamp(LocalDateTime.now()).build();

        return new ResponseEntity(detailErrorMessage,HttpStatus.BAD_REQUEST);
    }

}
