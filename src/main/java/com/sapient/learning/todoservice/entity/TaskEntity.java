package com.sapient.learning.todoservice.entity;
import java.time.LocalDateTime;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.mongodb.core.mapping.Document;

import lombok.Data;

/**
 * @author - chiranjitbhattacharya
 * @created - 13/07/2021
 */

@Data
@Document( collection = "tasks")
public class TaskEntity {

    @Id
    private ObjectId _id;
    private String taskId;
    private String title;
    private String category;
    private String status;
    
    private String username;

    @CreatedDate
    private LocalDateTime createdOn;
    @LastModifiedDate
    private LocalDateTime modifiedOn;

}
