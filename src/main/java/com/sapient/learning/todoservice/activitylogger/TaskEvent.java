package com.sapient.learning.todoservice.activitylogger;

import java.time.LocalDateTime;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskEvent {
	
	private EventType eventType;
	private String taskId;
	private String taskTitle;
	private LocalDateTime timestamp;
	
	private String username;

}
