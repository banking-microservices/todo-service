package com.sapient.learning.todoservice.activitylogger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class ActivityPublisher {
	
	@Value("${activity.logger.topic.name}")
	private String topic;
	
	@Autowired
	private KafkaTemplate<String, TaskEvent> publisher;
	
	
	@Async
	public void publish(TaskEvent event) {
		System.out.println("->>>>>>> logging...");
		publisher.send(topic, event);
	}

}
