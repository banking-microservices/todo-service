package com.sapient.learning.todoservice.activitylogger;

public enum EventType {
	
	TASK_CREATED,
	TASK_UPDATED,
	TASK_DELETED;

}
