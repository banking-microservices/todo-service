package com.sapient.learning.todoservice.util;

import com.sapient.learning.todoservice.dto.TaskDto;
import com.sapient.learning.todoservice.entity.TaskEntity;
import org.modelmapper.ModelMapper;

/**
 * @author - chiranjitbhattacharya
 * @created - 13/07/2021
 */
public class ConvertorFunctions {

    private static final ModelMapper modelMapper = new ModelMapper();

    private ConvertorFunctions() {
        // To hide the implicit one.
    }

    public static TaskEntity toTaskEntity(TaskDto taskDto) {
       return modelMapper.map(taskDto, TaskEntity.class);
    }

    public static TaskDto toTaskDto(TaskEntity taskEntity) {
        return modelMapper.map(taskEntity, TaskDto.class);
    }
}
