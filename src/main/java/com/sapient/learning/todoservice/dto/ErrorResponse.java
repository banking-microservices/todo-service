package com.sapient.learning.todoservice.dto;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @author - chiranjitbhattacharya
 * @created - 13/07/2021
 */
@Data
@Builder
public class ErrorResponse {

    private String path;
    private int code;
    private String status;
    private String message;
    private LocalDateTime timestamp;
    private List<FieldError> errors;
}
