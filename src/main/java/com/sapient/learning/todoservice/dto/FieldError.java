package com.sapient.learning.todoservice.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @author - chiranjitbhattacharya
 * @created - 13/07/2021
 */
@Data
@AllArgsConstructor
public class FieldError {

    private String field;
    private String errorMessage;
}
