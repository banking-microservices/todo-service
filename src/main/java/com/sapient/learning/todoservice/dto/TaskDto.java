package com.sapient.learning.todoservice.dto;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;
import org.bson.types.ObjectId;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

/**
 * @author - chiranjitbhattacharya
 * @created - 13/07/2021
 */
@Data
public class TaskDto {

    @Schema( example = "5aecef5b6d55754834124df3", description = "Unique Id for the task", required = false)
    @JsonSerialize(using= ToStringSerializer.class)
    private ObjectId _id;

    @Schema( example = "Order Pizza", description = "Tile of the task", required = true)
    @NotNull( message = "{taskDto.title.null}")
    @NotEmpty( message = "Task title can't be empty.")
    private String title;

    @Schema( example = "Personal", description = "Category of the task", required = true)
    private String category;

    @Schema( example = "OPEN", description = "Current status of the task", required = false)
    private String status;
    
    @Schema( example = "jack", description = "Username", required = true)
    private String username;
    

    @Schema( example = "2021-07-13T15:36:24.699Z", description = "Task created time", required = false)
    private LocalDateTime createdOn;

    @Schema( example = "2021-07-13T15:36:24.699Z", description = "Task last modified time", required = false)
    private LocalDateTime modifiedOn;
}
